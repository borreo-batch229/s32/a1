let http = require("http");

http.createServer(function(req,res){
		if(req.url == "/profile" && req.method == "GET"){
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.write("Welcome to our profile!");
		res.end();
	
	}else if(req.url == "/courses" && req.method == "GET"){
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.write("Here's our courses available");
		res.end();
	
	}else if(req.url == "/addcourse" && req.method == "GET"){
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.write("Add a course to our resources");
		res.end();
	
	}else if(req.url == "/updatecourse" && req.method == "GET"){
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.write("Update a course to our resources");
		res.end();
	
	}else if(req.url == "/archivecourses" && req.method == "GET"){
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.write("Archive courses to our resources");
		res.end();
	
	}else{
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.write("Welcome to Booking System");
		res.end();
	}
	
}).listen(4000);